/*:simple_positional */
SELECT * FROM foo WHERE id = ? AND foo = ? AND bar = ?

/*:simple_named */
SELECT * FROM foo WHERE id = :id AND foo = :foo AND bar = :bar

/*:selectOne */
SELECT 1

/*:namespace.one */
SELECT * FROM foo
/*:namespace.two */
SELECT * FROM foo

/*:multilineComment */
/*
:not_a_parameter
? < not a parameter
*/
SELECT * FROM foo WHERE id = :id

/*:numberSignComment */
SELECT * FROM foo WHERE # uuid = :uuid
id = :id

/*:doubleDashComment */
SELECT * FROM foo WHERE -- uuid = :uuid
id = ?

/*:singleQuote */
SELECT * FROM foo WHERE id = "?" OR uuid = ":uuid"

/*:doubleQuote */
SELECT * FROM foo WHERE id = '?' OR uuid = ':uuid'

/*:escapedQuotes */
SELECT * FROM foo WHERE id = ':one \' :two' OR uuid = ":three \" :four"

/*:repeatedParameter */
SELECT * FROM foo WHERE id > :id OR id < :id

/*:escapedSlash */
SELECT * FROM table
WHERE
  name = "bla \\" AND uuid = :uuid AND id = :id

/*:quotedComment */
  SELECT * FROM foo
  WHERE
    bar = " # :numberComment"
    OR
    foo = " -- :dashComment"
