var assert = require('assert'),
	  parseQuery = require('./parser');

// const PLACEHOLDER_NONE = 0;
// const PLACEHOLDER_POSITIONAL = 1;
var PLACEHOLDER_NAMED = 2;

/**
 * Parse a query and return a function to use it
 * @param {String} query SQL query
 * @param {Object} [db] mysql/pg/any-db adapter
 * @return {query}
 */
function prepare(queryName, query, opts) {
    var db = opts.db, promisifier = opts.promisify;
	  assert(queryName && typeof queryName === 'string', 'Missing queryName');
	  assert(query && typeof query === 'string', 'Missing queryStr');
	  assert(!db || typeof db === 'object' && typeof db.query === 'function', 'Invalid db');
	  assert(!promisifier || typeof promisifier === 'function', 'Invalid promisifier');

	  var parsedQuery = parseQuery(query);

	  /**
	   * @function query
	   * @param {Object} [connection]
	   * @param {(Object|Array)} [parameters]
	   * @param {Function} [callback]
	   */
	  var fn = function() {
		    var connection, parameters, callback;
		    switch(arguments.length) {
			  case 3:
				    connection = arguments[0];
				    parameters = arguments[1];
				    callback = arguments[2];
				    break;
			  case 2:
				    if (typeof arguments[0] === 'object' && typeof arguments[0].query === 'function') {
					      connection = arguments[0];
					      if (typeof arguments[1] === 'function') {
						        callback = arguments[1];
					      } else {
						        parameters = arguments[1];
					      }
				    } else {
					      parameters = arguments[0];
					      callback = arguments[1];
				    }
				    break;
			  case 1:
				    if (typeof arguments[0] === 'object' && typeof arguments[0].query === 'function') {
					      connection = arguments[0];
				    } else if (typeof arguments[0] === 'function') {
					      callback = arguments[0];
				    } else {
					      parameters = arguments[0];
				    }
				    break;
		    }

		    if (!connection) connection = db;

		    assert(!callback || typeof callback === 'function', 'Invalid callback');

		    try {
			      assert(connection, 'Missing DB connection');
			      assert(typeof connection.query === 'function', 'Invalid DB connection');

			      var params = parameters;
			      if (parsedQuery.type === PLACEHOLDER_NAMED) {
				        assert(typeof params === 'object', 'Invalid parameters type');
				        params = parsedQuery.params.map(function(key) {
					          assert(params.hasOwnProperty(key), 'Query parameter "' + key + '" is missing');
					          return params[key];
				        });
			      }
			      return connection.query(parsedQuery.query, params, callback);
		    } catch (err) {
			      if (callback) {
				        callback(err);
			      } else {
				        throw err;
			      }
		    }
	  };
	  fn.name = fn.displayName = 'jsql.' + queryName;
	  return promisifier ? promisifier(fn) : fn;
}

/**
 * Recursively loops through each nodes of a query file and creates functions
 * @param {Object} node A query file node
 * @param {Object} [db] A mysql/pg/any-db adapter
 */
function construct(node, opts, queryName) {
	  queryName = queryName || '';
	  return Object.keys(node).reduce(function(stack, key) {
		    if (typeof node[key] === 'string') {
			      stack[key] = prepare(queryName + key, node[key], opts);
		    } else {
			      stack[key] = construct(node[key], opts, queryName + key + '.');
		    }
		    return stack;
	  }, {});
}

module.exports = {
	  construct: construct,
	  prepare: prepare
};
