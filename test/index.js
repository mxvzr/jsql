var fs = require('fs'),
	  should = require('should'),
	  anyDB = require('any-db'),
	  promisify = require('bluebird').promisify,
	  db = anyDB.createConnection('sqlite3://:memory'),
    filepath = 'file://' + __dirname + '/data/sample.sql',
	  jsql = require('../lib/index');

const PLACEHOLDER_NONE = 0;
const PLACEHOLDER_POSITIONAL = 1;
const PLACEHOLDER_NAMED = 2;

describe('jsql', function() {

	  it('works with traditional callbacks...', function(done) {
		    var queries = jsql.loadSync(filepath, {db: db});
        queries.selectOne(function(err, result) {
			      should.not.exist(err);
			      should.exist(result);
			      result.should.have.property('rows').is.instanceof.Array;
			      result.rows.length.should.equal(1);
			      result.rows[0].should.have.property('1').equal(1);
			      result.should.have.property('fields');
			      result.should.have.property('rowCount').equal(1);
			      result.should.have.property('lastInsertId');
			      done();
		    });
	  });

	  it('... and supports an optional promisifier', function(done) {
		    var queries = jsql.loadSync(filepath, {db: db, promisify: promisify});
        queries.selectOne()
			      .then(function(result) {
				        result.should.have.property('rows').is.instanceof.Array;
				        result.rows.length.should.equal(1);
				        result.rows[0].should.have.property('1').equal(1);
				        result.should.have.property('fields');
				        result.should.have.property('rowCount').equal(1);
				        result.should.have.property('lastInsertId');
				        done();
			      });
	  });

	  it('can use a custom DB at calltime', function(done) {
		    var queries = jsql.loadSync(filepath, {promisify: promisify});
		    queries.selectOne(db)
			      .then(function(result) {
				        result.should.have.property('rows').is.instanceof.Array;
				        result.rows.length.should.equal(1);
				        result.rows[0].should.have.property('1').equal(1);
				        result.should.have.property('fields');
				        result.should.have.property('rowCount').equal(1);
				        result.should.have.property('lastInsertId');
				        done();
			      });
	  });

	  it('also works asynchronously', function(done) {
		    var queries = jsql.load(filepath, {db:db}, function(err, queries) {
			      queries.selectOne(function(err, result) {
				        should.not.exist(err);
				        should.exist(result);
				        result.should.have.property('rows').is.instanceof.Array;
				        result.rows.length.should.equal(1);
				        result.rows[0].should.have.property('1').equal(1);
				        result.should.have.property('fields');
				        result.should.have.property('rowCount').equal(1);
				        result.should.have.property('lastInsertId');
				        done();
			      });
		    });
	  });

	  it('DBs passed at calltime are used over DBs passed at build time', function(done) {
		    var buildDB = {
			      query: function() {
				        var args = Array.prototype.slice.call(arguments),
					          callback = args.pop(),
					          params = args.pop();

				        callback(new Error('FAIL'), params);
			      }
		    };
		    var callbDB = {
			      query: function() {
				        var args = Array.prototype.slice.call(arguments),
					          callback = args.pop(),
					          params = args.pop();

				        callback(null, params);
			      }
		    };
		    var queries = jsql.loadSync(filepath, buildDB);
		    queries.simple_named(callbDB, {
			      id: "id",
			      foo: "FOO",
			      bar: "BAR"
		    }, done);
	  });
});
