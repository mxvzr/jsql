var assert = require('assert'),
	  fs = require('fs'),
	  path = require('path'),
	  prepare = require('./preparator').prepare,
	  construct = require('./preparator').construct,
    StateMachine = require('state-machine');

var DELIMITER_START = '/*:';
var DELIMITER_END = '*/';

var VALID_IDENTIFIER = '[a-z$_][a-z0-9$_]*';
var VALID_QUERY_NAME = '^' + VALID_IDENTIFIER + '(.' + VALID_IDENTIFIER + ')*$';

var VALID_JS_NAME_RE = /^[a-z\$_][a-z0-9\$_]*(.[a-z\$_][a-z0-9-\$_]*)*$/gi;

function parseFile(str) {
	  var position = 0,
		    queries = [],
		    stateMachine = StateMachine();

	  stateMachine.build()
		    .state('none', {initial: true})
		    .state('inDelimiter')
		    .state('inQuery')
		    .event('delimiterStart', 'none', 'inDelimiter')
		    .event('delimiterStart', 'inQuery', 'inDelimiter')
		    .event('delimiterEnd', 'inDelimiter', 'inQuery');

	  function scan() {
		    var next,
			      state = stateMachine.currentState();

		    switch (state) {
			  case 'inQuery':
			  case 'none':
				    next = str.indexOf(DELIMITER_START, position);
				    if (next === -1) {
					      if (state === 'inQuery') // EOF
						        queries[queries.length - 1].str += str.substring(position);
					      position = str.length; // stop processing
				    } else {
					      if (state === 'inQuery')
						        queries[queries.length - 1].str += str.substring(position, next);
					      stateMachine.delimiterStart();
					      position = next;
				    }
				    break;
			  case 'inDelimiter':
				    next = str.indexOf(DELIMITER_END, position);
				    assert(next > -1, 'Could not find the end of delimiter');
				    queries.push({
					      name: str.substring(position + DELIMITER_START.length, next),
					      str: ''
				    });
				    stateMachine.delimiterEnd();
				    position = next + DELIMITER_END.length;
				    break;
		    }
	  }

	  while (position < str.length-1)
		    scan();

	  if (!queries.length)
		    return null;

	  return queries.map(function(query) {
        query.name = query.name.trim();
		    query.str = query.str.trim();
        assert(new RegExp(VALID_QUERY_NAME, 'gi').test(query.name), 'Invalid queryName: ' + query.name);
/*        assert(
            VALID_JS_NAME_RE.test(query.name),
            'Invalid queryName: ' + query.name
        );
        */
		    return query;
	  }).reduce(function(stack, query) {
		    var parts = query.name.split('.');
		    if (parts.length === 1) {
			      assert(!stack.hasOwnProperty(query.name), 'Duplicate query name: ' + query.name);
			      stack[query.name] = query.str;
		    } else {
			      var accessor = stack;
			      parts.forEach(function(part, index, arr) {
				        if (index < arr.length - 1) {
					          if (!accessor.hasOwnProperty(part))
						            accessor[part] = {};
					          accessor = accessor[part];
				        } else {
					          assert(!accessor.hasOwnProperty(part), 'Duplicate query name: ' + query.name);
					          accessor[part] = query.str;
				        }
			      });
		    }
		    return stack;
	  }, {});
}

/**
 * @param {String} filepath the path to the SQL file containing a single SQL query
 * @param {?Object} db A mysql/pg/any-db adapter
 * @param {?Function} promisifier A promisifier function (or cache layer)
 * @param {Function} callback
 */
function loadQueryFile(filepath, opts, callback) {
    fs.readFile(filepath, {encoding: 'utf-8'}, function(err, data) {
		    if (err) {
			      callback(err);
		    } else {
			      var queries = parseFile(data);
			      if (!queries) {
				        var query,
					      queryName = path.basename(filepath, '.sql');
				        try {
					          query = prepare(queryName, data, opts);
				        } catch(e) {
					          return callback(e);
				        }
				        callback(null, query);
			      } else {
				        try {
					          queries = construct(queries, opts);
				        } catch(e) {
					          return callback(e);
				        }
				        callback(null, queries);
			      }
		    }
	  });
}

/**
 * @param {String} filepath the path to the yaml file containing a single SQL query
 * @param {?Object} db] A mysql/pg/any-db adapter
 * @param {?Function} promisifier A promisifier function (or cache layer)
 * @return {Object.<string, function|object>}
 */
function loadQueryFileSync(filepath, opts) {
    opts = opts || {};
    var data =  fs.readFileSync(filepath, {encoding: 'utf-8'}),
		queries = parseFile(data);

	  if (!queries) {
		    var queryName = path.basename(filepath, '.sql');
		    return prepare(queryName, data, opts);
	  } else {
		    return construct(queries, opts);
	  }
}

module.exports = {
	  loadQueryFile: loadQueryFile,
	  loadQueryFileSync: loadQueryFileSync
};
