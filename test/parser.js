var fs = require('fs'),
	  should = require('should'),
	  parser = require('../lib/parser'),
    yaml = require('js-yaml'),
	  queries = yaml.safeLoad(fs.readFileSync(__dirname + '/data/sample.yaml'));

const PLACEHOLDER_NONE = 0;
const PLACEHOLDER_POSITIONAL = 1;
const PLACEHOLDER_NAMED = 2;

describe('parser', function() {

	describe('simple_positional', function() {
		var result = parser(queries.simple_positional);
		it('parses the right number of parameters', function() {
			result.length.should.equal(3);
		});
		it('returns the correct query type', function() {
			result.type.should.equal(PLACEHOLDER_POSITIONAL);
		});
	});

	describe('simple_named', function() {
		var result = parser(queries.simple_named);
		it('parses the right number of parameters', function() {
			result.length.should.equal(3);
		});
		it('returns the correct query type', function() {
			result.type.should.equal(PLACEHOLDER_NAMED);
		});
		it('returns the parameter names', function() {
			should.exists(result.params);
			result.params.should.be.an.Array;
			result.params.should.containEql('id');
			result.params.should.containEql('foo');
			result.params.should.containEql('bar');
		});
	});

	describe('multilineComment', function() {
		var result = parser(queries.multilineComment);
		it('parses the right number of parameters', function() {
			result.length.should.equal(1);
		});
		it('returns the correct query type', function() {
			result.type.should.equal(PLACEHOLDER_NAMED);
		});
		it('returns the parameter names', function() {
			should.exists(result.params);
			result.params.should.be.an.Array;
			result.params.should.containEql('id');
		});
	});

	describe('numberSignComment', function() {
		var result = parser(queries.numberSignComment);
		it('parses the right number of parameters', function() {
			result.length.should.equal(1);
		});
		it('returns the correct query type', function() {
			result.type.should.equal(PLACEHOLDER_NAMED);
		});
		it('returns the parameter names', function() {
			should.exists(result.params);
			result.params.should.be.an.Array;
			result.params.should.containEql('id');
		});
	});

	describe('doubleDashComment', function() {
		var result = parser(queries.doubleDashComment);
		it('parses the right number of parameters', function() {
			result.length.should.equal(1);
		});
		it('returns the correct query type', function() {
			result.type.should.equal(PLACEHOLDER_POSITIONAL);
		});
	});

	describe('singleQuote', function() {
		var result = parser(queries.singleQuote);
		it('parses the right number of parameters', function() {
			result.length.should.equal(0);
		});
		it('returns the correct query type', function() {
			result.type.should.equal(PLACEHOLDER_NONE);
		});
	});

	describe('doubleQuote', function() {
		var result = parser(queries.doubleQuote);
		it('parses the right number of parameters', function() {
			result.length.should.equal(0);
		});
		it('returns the correct query type', function() {
			result.type.should.equal(PLACEHOLDER_NONE);
		});
	});

	describe('escapedQuotes', function() {
		var result = parser(queries.escapedQuotes);
		it('parses the right number of parameters', function() {
			result.length.should.equal(0);
		});
		it('returns the correct query type', function() {
			result.type.should.equal(PLACEHOLDER_NONE);
		});
	});

	describe('quotedComment', function() {
		var result = parser(queries.quotedComment);
		it('parses the right number of parameters', function() {
			result.length.should.equal(0);
		});
		it('returns the correct query type', function() {
			result.type.should.equal(PLACEHOLDER_NONE);
		});
	});

	describe('repeatedParameter', function() {
		var result = parser(queries.repeatedParameter);
		it('parses the right number of parameters', function() {
			result.length.should.equal(2);
		});
		it('returns the correct query type', function() {
			result.type.should.equal(PLACEHOLDER_NAMED);
		});
	});

	describe('mixedStyle', function() {
		it('throw an error', function() {
			(function() {
				parser('SELECT * FROM foo WHERE id = ? OR uuid = :uuid');
			}).should.throw();
		});
	});

	describe('escaped slash', function() {
		var result = parser(queries.escapedSlash);
		it('parses the right number of parameters', function() {
			result.length.should.equal(2);
		});
		it('returns the correct query type', function() {
			result.type.should.equal(PLACEHOLDER_NAMED);
		});
		it('returns the parameter names', function() {
			should.exists(result.params);
			result.params.should.be.an.Array;
			result.params.should.containEql('id');
			result.params.should.containEql('uuid');
		});
	});

});

