var path = require('path'),
	  assert = require('assert'),
    file = require('./file'),
    prepare = require('./preparator').prepare;

var FILE_URI = /^file:\/\//;

function assertDeprecatedExtensions(filepath) {
    var ext = path.extname(filepath);
    if (['.yaml', '.yml'].indexOf(ext) > -1)
        throw new Error('Deprecated extension: ' + ext);
}

function assertValidOpts(opts) {
    assert(
        !opts.hasOwnProperty('db') || typeof opts.db.query === 'function',
        'Invalid opts.db value'
    );
    assert(
        !opts.hasOwnProperty('promisify') || typeof opts.promisify === 'function',
        'Invalid opts.promisify value'
    );
}

function load(filepath, opts, callback) {
    opts = opts || {};
    if (FILE_URI.test(filepath)) {
        loadQueryFile(filepath.substr(7), opts, callback);
    } else {
        callback(null, prepare('inline', filepath, opts));
    }
}

function loadSync(filepath, opts) {
    opts = opts || {};
    if (FILE_URI.test(filepath)) {
        return loadQueryFileSync(filepath.substr(7), opts);
    } else {
        return prepare('inline', filepath, opts);
    }
}

/**
 * @param {String} filepath the path to the queryFile
 * @param {Object} [db] A mysql/pg/any-db adapter
 * @param {Function} [promisifier] A promisifier function (or cache layer)
 * @param {Function} callback
 */
function loadQueryFile(filepath, opts, callback) {
    try {
        assertDeprecatedExtensions(filepath);
        assertValidOpts(opts);
    } catch (err) {
        callback(err);
        return;
    }
    file.loadQueryFile(filepath, opts, callback);
}

/**
 * @param {String} filepath the path to the queryFile
 * @param {Object} [db] A mysql/pg/any-db adapter
 * @param {Function} [promisifier] A promisifier function (or cache layer)
 * @return {Object.<string, function|object>}
 */
function loadQueryFileSync(filepath, opts) {
    assertDeprecatedExtensions(filepath);
    assertValidOpts(opts);
	  return file.loadQueryFileSync(filepath, opts);
}

module.exports = {
    load: load,
    loadSync: loadSync,
	  loadQueryFile: loadQueryFile,
	  loadQueryFileSync: loadQueryFileSync,
    promisify: function(fn) {
        return function() {
            var args = Array.prototype.slice.call(arguments);
            var callback = args.pop();
            return new Promise(function(resolve, reject) {
                args.push(function(err) {
                    if (err) {
                        reject(err);
                    } else {
                        var args = Array.prototype.slice.call(arguments);
                        args.shift();
                        resolve.apply(null, args);
                    }
                });
                fn.apply(null, args);
            });
        };
    }
};
