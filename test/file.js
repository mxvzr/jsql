var should = require('should'),
	  sql = require('../lib/file'),
	  filepath = __dirname + '/data/sample.sql';

describe('file', function() {

	  it('parses a sql query file properly', function() {
		    var queries = sql.loadQueryFileSync(filepath);
		    queries.simple_positional.should.be.a.Function;
		    queries.simple_named.should.be.a.Function;
		    queries.namespace.should.be.an.Object;
		    queries.namespace.one.should.be.a.Function;
		    queries.namespace.two.should.be.a.Function;
	  });

	  it('asynchronously parses a sql query file properly', function(done) {
		    var queries = sql.loadQueryFile(filepath, {}, function(err, queries) {
			      should.not.exist(err);
			      should.exist(queries);
			      queries.simple_positional.should.be.a.Function;
			      queries.simple_named.should.be.a.Function;
			      queries.namespace.should.be.an.Object;
			      queries.namespace.one.should.be.a.Function;
			      queries.namespace.two.should.be.a.Function;
			      done();
		    });
	  });

	  it('parses a sql file containing a single query properly', function() {
		    var query = sql.loadQueryFileSync(__dirname + '/data/single.sql', {});
		    query.should.be.a.Function;
	  });

});
