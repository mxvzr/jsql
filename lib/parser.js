var assert = require('assert'),
	  stateMachine = require('state-machine');

var PLACEHOLDER_NONE = 0;
var PLACEHOLDER_POSITIONAL = 1;
var PLACEHOLDER_NAMED = 2;

/**
 * @constructor
 * @private
 * @param name {String} The parameter's name
 * @param position {Number} The parameter's position in the queryStr
 */
function Placeholder(name, position) {
	  this.name = name.substr(1);
	  this.position = position;
	  this.length = name.length;
	  this.end = position + name.length;
}

/**
 * @constructor
 * @param queryStr {String} The query string
 */
function Parser(queryStr, pgsqlParams) {
	  this.query = queryStr;
	  this.stateMachine = stateMachine();
	  this.position = -1;
	  this.placeholderStyle = PLACEHOLDER_NONE;
	  this.parsedPlaceholderCount = 0;
	  this.parsedPlaceholders = [];
	  this.stateMachine.build()
		    .state('query', {initial: true})
		    .state('commented')
		    .state('multilineCommented')
		    .state('quoted')
		    .state('doubleQuoted')
		    .event('numberSign', 'query', 'commented')
		    .event('doubleDash', 'query', 'commented')
		    .event('newLine', 'commented', 'query')
		    .event('multiLineStart', 'query', 'multilineCommented')
		    .event('multiLineEnd', 'multilineCommented', 'query')
		    .event('singleQuote', 'query', 'quoted')
		    .event('singleQuote', 'quoted', 'query')
		    .event('doubleQuote', 'query', 'doubleQuoted')
		    .event('doubleQuote', 'doubleQuoted', 'query');

	  while (this.position < this.query.length-1)
		    this.scan();

	  var query = this.query, params;

	  if (this.placeholderStyle === PLACEHOLDER_NAMED) {
		    var offset = 0;
		    params = this.parsedPlaceholders.map(function(placeholder, index) {
            if (!pgsqlParams) {
                query = query.substring(0, placeholder.position + offset) + 
				            '?' + query.substring(placeholder.end + offset);
			          offset -= placeholder.length - 1;
            } else {
                index = String(index + 1);
                query = query.substring(0, placeholder.position + offset) +
                    '$' + index + query.substring(placeholder.end + offset);
                offset -= index.length + (placeholder.length - 1);
            }
			      return placeholder.name;
		    });
	  }

	  var retVal = {
		    query: query,
		    length: this.parsedPlaceholderCount,
		    type: this.placeholderStyle
	  };

	  if (params)
		    retVal.params = params;

	  return retVal;
}

/**
 * Read the character at the current position
 */
Parser.prototype.read = function() {
	  return this.query[this.position];
};

/**
 * Read the character at the next position
 */
Parser.prototype.peek = function() {
	  return this.query[this.position + 1];
};

/**
 * Scan the next character individually
 */
Parser.prototype.scan = function() {
	  this.position++;

	  var character = this.read();
	  switch (this.stateMachine.currentState()) {
		case 'query':
			  switch (character) {
				case '"':
					  this.stateMachine.doubleQuote();
					  break;
				case '\'':
					  this.stateMachine.singleQuote();
					  break;
				case '/':
					  if (this.peek() === '*') {
						    this.position++;
						    this.stateMachine.multiLineStart();
					  }
					  break;
				case '#':
					  this.stateMachine.numberSign();
					  break;
				case '-':
					  if (this.peek() === '-') {
						    this.position++;
						    this.stateMachine.doubleDash();
					  }
					  break;
				case '?':
					  assert(this.placeholderStyle !== PLACEHOLDER_NAMED, 'Placeholder styles cannot be mixed');
					  this.placeholderStyle = PLACEHOLDER_POSITIONAL;
					  this.parsedPlaceholderCount++;
					  break;
				case ':':
					  assert(this.placeholderStyle !== PLACEHOLDER_POSITIONAL, 'Placeholder styles cannot be mixed');
					  this.placeholderStyle = PLACEHOLDER_NAMED;
					  var name = '';
					  for (var i = this.position; i < this.query.length; i++) {
						    if (!/[:a-zA-Z0-9_]/.test(this.query[i]))
							      break;
						    name += this.query[i];
					  }
					  this.parsedPlaceholders.push(new Placeholder(name, this.position));
					  this.parsedPlaceholderCount++;
					  this.position += name.length;
					  break;
			  }
			  break;
		case 'commented':
			  if (character === '\n') {
				    this.stateMachine.newLine();
			  }
			  break;
		case 'multilineCommented':
			  if (character === '*' && this.peek() === '/') {
				    this.position++;
				    this.stateMachine.multiLineEnd();
			  }
			  break;
		case 'quoted':
			  if (character === '\'') {
				    this.stateMachine.singleQuote();
			  } else if (character === '\\' && ['\'', '\\'].indexOf(this.peek()) > -1) {
				    this.position++;
			  }
			  break;
		case 'doubleQuoted':
			  if (character === '"') {
				    this.stateMachine.doubleQuote();
			  } else if (character === '\\' && ['"', '\\'].indexOf(this.peek()) > -1) {
				    this.position++;
			  }
			  break;
	  }
};

module.exports = function(query) {
	  return new Parser(query);
};
